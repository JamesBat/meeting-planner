package com.meeting.planner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.meeting.planner.dtos.ToolDto;
import com.meeting.planner.entities.ToolEntity;
import com.meeting.planner.exceptions.MeetingPlannerException;
import com.meeting.planner.mappers.ToolMapper;
import com.meeting.planner.repositories.ToolRepository;
import com.meeting.planner.services.ToolServiceImpl;

@ExtendWith(MockitoExtension.class)
class ToolServiceTest {

	@Mock ToolMapper toolMapper;
	@Mock ToolRepository toolRepository;
	@InjectMocks ToolServiceImpl toolService;
	@Captor ArgumentCaptor<ToolEntity> toolArgumentCaptor;
	
	@Test
	void test_add() {
		ToolDto dto = new ToolDto(null, "webcam", 5);
		ToolEntity entity = new ToolEntity(null, "webcam", 5, null);
		ToolEntity entityResult = new ToolEntity(1, "webcam", 5, null);
		ToolDto dtoResult = new ToolDto(1, "webcam", 5);
		
		when(toolRepository.findByName(dto.getName())).thenReturn(null);
		when(toolMapper.dtoToEntity(dto)).thenReturn(entity);
		when(toolRepository.save(entity)).thenReturn(entityResult);
		when(toolMapper.entityToDto(entityResult)).thenReturn(dtoResult);
		
		ToolDto result = toolService.add(dto);
		
		verify(toolRepository, Mockito.times(1)).findByName(dto.getName());
		assertNotNull(result.getId());
		assertEquals(dto.getName(), result.getName());
	}
	
	@Test
	void test_add_whenToolAlreadyExist() {
		ToolDto dto = new ToolDto(null, "webcam", 5);
		ToolEntity entity = new ToolEntity(3, "webcam", 5, null);
		
		when(toolRepository.findByName(dto.getName())).thenReturn(entity);
		
		MeetingPlannerException exception = assertThrows(MeetingPlannerException.class, () -> toolService.add(dto), "Unexpected error");
		
		verify(toolRepository, Mockito.times(1)).findByName(dto.getName());
		assertEquals("Cet outil esxiste deja", exception.getMessage());
	}
	
	@Test
	void test_getAll() {
		List<ToolEntity> entities = new ArrayList<>();
		entities.add(new ToolEntity(null, "webcam", 5, null));
		entities.add(new ToolEntity(null, "tableau", 3, null));
		entities.add(new ToolEntity(null, "pieuvre", 2, null));
		
		when(toolRepository.findAll()).thenReturn(entities);
		
		List<ToolDto> dtos = toolService.getAll();
		
		verify(toolRepository, Mockito.times(1)).findAll();
		assertEquals(entities.size(), dtos.size());
	}
	
	@Test
	void test_update() {
		Integer id = 3;
		ToolDto dto = new ToolDto(3, "webcam", 6);
		ToolEntity entity = new ToolEntity(3, "webcam", 3, null);
		ToolEntity entityResult = new ToolEntity(3, "webcam", 6, null);
		
		when(toolRepository.findById(id)).thenReturn(Optional.of(entity));
		when(toolMapper.updateTool(dto, entity)).thenReturn(entityResult);
		when(toolRepository.save(entityResult)).thenReturn(entityResult);
		when(toolMapper.entityToDto(entityResult)).thenReturn(dto);
		
		ToolDto result = toolService.update(entity.getId(), dto);
		
		verify(toolRepository, Mockito.times(1)).findById(id);
		assertEquals(dto.getId(), result.getId());
		assertEquals(dto.getName(), result.getName());
	}
	
	@Test
	void test_getByName() {
		String name = "webcam";
		ToolEntity entity = new ToolEntity(3, "webcam", 5, null);
		ToolDto dto = new ToolDto(3, "webcam", 5);
		
		when(toolRepository.findByName(name)).thenReturn(entity);
		when(toolMapper.entityToDto(entity)).thenReturn(dto);
		
		ToolDto result = toolService.getByName(name);
		
		verify(toolRepository, Mockito.times(1)).findByName(dto.getName());
		assertEquals(result.getName(), name);
	}

}
