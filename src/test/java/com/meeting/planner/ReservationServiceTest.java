package com.meeting.planner;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.meeting.planner.entities.ReservationEntity;
import com.meeting.planner.mappers.ReservationMapper;
import com.meeting.planner.repositories.ReservationRepository;
import com.meeting.planner.services.ReservationServiceImpl;

@ExtendWith(MockitoExtension.class)
class ReservationServiceTest {

	@Mock ReservationMapper reservationMapper;
	@Mock ReservationRepository reservationRepository;
	@InjectMocks ReservationServiceImpl reservationService;
	@Captor ArgumentCaptor<ReservationEntity> reservationArgumentCaptor;

	@Test
	void test() {
		
	}

}
