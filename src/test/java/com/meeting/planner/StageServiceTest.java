package com.meeting.planner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.meeting.planner.dtos.StageDto;
import com.meeting.planner.entities.StageEntity;
import com.meeting.planner.mappers.StageMapper;
import com.meeting.planner.repositories.StageRepository;
import com.meeting.planner.services.StageServiceImpl;

@ExtendWith(MockitoExtension.class)
class StageServiceTest {
	
	@Mock StageRepository stageRepository;
	@Mock StageMapper stageMapper;
	@InjectMocks StageServiceImpl stageService;
	@Captor ArgumentCaptor<StageEntity> stageArgumentCaptor;

	@Test
	void test_getAll() {
		List<StageEntity> entities = new ArrayList<>();
		entities.add(new StageEntity(1, "E1", null));
		entities.add(new StageEntity(2, "E2", null));
		
		when(stageRepository.findAll()).thenReturn(entities);
		
		List<StageDto> dtos = stageService.getAll();
		
		verify(stageRepository, Mockito.times(1)).findAll();
		assertEquals(entities.size(), dtos.size());
	}
}
