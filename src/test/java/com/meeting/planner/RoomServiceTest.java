package com.meeting.planner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.meeting.planner.dtos.RoomDto;
import com.meeting.planner.entities.RoomEntity;
import com.meeting.planner.entities.StageEntity;
import com.meeting.planner.mappers.RoomMapper;
import com.meeting.planner.repositories.RoomRepository;
import com.meeting.planner.services.RoomServiceImpl;

@ExtendWith(MockitoExtension.class)
class RoomServiceTest {

	@Mock RoomMapper roomMapper;
	@Mock RoomRepository roomRepository;
	@InjectMocks RoomServiceImpl roomService;
	@Captor ArgumentCaptor<RoomEntity> roomArgumentCaptor;
	
	@Test
	void test_getAll() {
		StageEntity stage1 = new StageEntity(1,"E1",null);
		List<RoomEntity> entities = new ArrayList<>();
		entities.add(new RoomEntity(1, "E1001", 23, stage1, null, null));
		entities.add(new RoomEntity(null, "E1002", 10, stage1, null, null));

		when(roomRepository.findAll()).thenReturn(entities);
		
		List<RoomDto> dtos = roomService.getAll();
		
		verify(roomRepository, Mockito.times(1)).findAll();
		assertEquals(entities.size(), dtos.size());
	}
	

	
}
