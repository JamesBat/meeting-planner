package com.meeting.planner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.meeting.planner.dtos.MeetingDto;
import com.meeting.planner.entities.MeetingEntity;
import com.meeting.planner.enums.MeetingTypeEnum;
import com.meeting.planner.mappers.MeetingMapper;
import com.meeting.planner.repositories.MeetingRepository;
import com.meeting.planner.services.MeetingServiceImpl;

@ExtendWith(MockitoExtension.class)
class MeetingServiceTest {
	
	@Mock MeetingMapper meetingMapper;
	@Mock MeetingRepository meetingRepository;
	@InjectMocks MeetingServiceImpl meetingService;
	@Captor ArgumentCaptor<MeetingEntity> meetingArgumentCaptor;

	@Test
	void test_getByname() {
		LocalDate date = LocalDate.now();
		String name = "Reunion 1";
		MeetingEntity entity = new MeetingEntity(2, "Reunion 1", date.atTime(9, 00), date.atTime(10, 00), 8, MeetingTypeEnum.VC, null);
		MeetingDto dto = new MeetingDto(2, "Reunion 1", date.atTime(9, 00), date.atTime(10, 00), 8, MeetingTypeEnum.VC);
		
		when(meetingRepository.findByName(name)).thenReturn(entity);
		when(meetingMapper.entityToDto(entity)).thenReturn(dto);

		MeetingDto result = meetingService.getByName(name);
		
		verify(meetingRepository, Mockito.times(1)).findByName(name);
		assertEquals(name, result.getName());
	}
	
	@Test
	void test_getAll() {
		LocalDate date = LocalDate.now();
		List<MeetingEntity> entities = new ArrayList<>();
		entities.add(new MeetingEntity(null, "Reunion 1", date.atTime(9, 00), date.atTime(10, 00), 8, MeetingTypeEnum.VC, null));
		entities.add(new MeetingEntity(null, "Reunion 2", date.atTime(9, 00), date.atTime(10, 00), 6, MeetingTypeEnum.SPEC, null));
		
		when(meetingRepository.findAll()).thenReturn(entities);
		
		List<MeetingDto> dtos = meetingService.getAll();
		
		verify(meetingRepository, Mockito.times(1)).findAll();
		assertEquals(entities.size(), dtos.size());
	}
	
	@Test
	void test_add() {
		LocalDate date = LocalDate.now();
		MeetingDto dto = new MeetingDto(null, "Reunion 1", date.atTime(9, 00), date.atTime(10, 00), 8, MeetingTypeEnum.VC);
		MeetingEntity entity = new MeetingEntity(null, "Reunion 1", date.atTime(9, 00), date.atTime(10, 00), 8, MeetingTypeEnum.VC, null);
		MeetingEntity entitySave = entity;
		entitySave.setId(1);
		MeetingDto dtoReturn = dto;
		dtoReturn.setId(1);
		
		when(meetingMapper.dtoToEntity(dto)).thenReturn(entity);
		when(meetingRepository.save(entity)).thenReturn(entitySave);
		when(meetingMapper.entityToDto(entitySave)).thenReturn(dtoReturn);
		
		MeetingDto result = meetingService.add(dto);
		
		verify(meetingRepository, Mockito.times(1)).save(entity);
		assertNotNull(result.getId());
	}
	
}
