package com.meeting.planner.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meeting.planner.entities.ReservationEntity;

@Repository
public interface ReservationRepository extends JpaRepository<ReservationEntity, Integer>{

	@Query("SELECT DISTINCT r FROM ReservationEntity r WHERE r.room.name = :roomName AND r.enabled = true AND r.startReservation = :startReservation AND r.endReservation = :endReservation")
	ReservationEntity getReservationByRoomParam(@Param("roomName") String roomName,@Param("startReservation") LocalDateTime startReservation,@Param("endReservation") LocalDateTime endReservation);

	@Query("SELECT DISTINCT r FROM ReservationEntity r WHERE r.room.name = :roomName AND r.enabled = true AND r.endReservation BETWEEN :startOfDay AND :endOfDay")
	List<ReservationEntity> getReservationByDate(@Param("roomName") String roomName,@Param("startOfDay") LocalDateTime startOfDay,@Param("endOfDay") LocalDateTime endOfDay);

	@Query("SELECT DISTINCT r FROM ReservationEntity r WHERE r.meeting.id = :id AND r.enabled = true")
	List<ReservationEntity> getReservationsByMeeting(@Param("id") Integer id);
	
	@Query("SELECT DISTINCT r FROM ReservationEntity r WHERE r.enabled = true")
	List<ReservationEntity> getAll();
	
	@Query("SELECT r FROM ReservationEntity r WHERE r.meeting.name = :meetingName AND r.enabled = true")
	ReservationEntity getReservationByMeetinName(@Param("meetingName") String meetingName);
}
