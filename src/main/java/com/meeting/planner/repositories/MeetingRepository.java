package com.meeting.planner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meeting.planner.entities.MeetingEntity;

@Repository
public interface MeetingRepository extends JpaRepository<MeetingEntity, Integer>{

	MeetingEntity findByName(String name);
}
