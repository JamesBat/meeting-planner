package com.meeting.planner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meeting.planner.entities.StageEntity;

@Repository
public interface StageRepository extends JpaRepository<StageEntity, Integer>{

	StageEntity findByName(String name);
}
