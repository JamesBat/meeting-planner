package com.meeting.planner.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meeting.planner.entities.RoomEntity;
import com.meeting.planner.entities.RoomToolEntity;

@Repository
public interface RoomToolRepository extends JpaRepository<RoomToolEntity, Integer>{

	@Query("SELECT DISTINCT rt.room FROM RoomToolEntity rt WHERE (rt.room.capacity*0.7) >= :capacity")
	List<RoomEntity> getRoomByParam(@Param("capacity") Integer capacity);
	
	@Query("SELECT DISTINCT rt.tool.name FROM RoomToolEntity rt WHERE rt.room.name = :name")
	List<String> getToolNameByRoomName(@Param("name") String name);
}
