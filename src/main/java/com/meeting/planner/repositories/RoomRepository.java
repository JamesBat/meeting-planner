package com.meeting.planner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meeting.planner.entities.RoomEntity;

@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, Integer>{
	RoomEntity findByName(String name);
}
