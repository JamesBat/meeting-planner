package com.meeting.planner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meeting.planner.entities.ToolEntity;

@Repository
public interface ToolRepository extends JpaRepository<ToolEntity, Integer>{

	ToolEntity findByName(String name);
}
