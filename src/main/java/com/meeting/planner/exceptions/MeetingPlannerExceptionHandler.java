package com.meeting.planner.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.ConstraintViolationException;

@ControllerAdvice
public class MeetingPlannerExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = {MeetingPlannerException.class })
	protected ResponseEntity<Object> handleApiException(MeetingPlannerException exc, WebRequest webRequest) {
		MeetingPlannerException apiException =  new MeetingPlannerException(
				exc.getMessage(),
				HttpStatus.NOT_ACCEPTABLE,
				exc.getLocalizedMessage(),
				null);
		return new ResponseEntity<>(apiException, HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(value = { EntityNotFoundException.class})
	protected ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException exc, WebRequest webRequest) {
		MeetingPlannerException apiException =  new MeetingPlannerException(
				exc.getLocalizedMessage(),
				HttpStatus.NOT_FOUND,
				ErrorsKeysConstants.ENTITY_NOT_FOUND,
				null);
		return new ResponseEntity<>(apiException, HttpStatus.NOT_FOUND);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		List<MeetingPlannerError> errors = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.add(new MeetingPlannerError(fieldName, errorMessage));
        });
        MeetingPlannerException apiException =  new MeetingPlannerException(
                ex.getMessage(),
                HttpStatus.BAD_REQUEST,
                ErrorsKeysConstants.DATA_VALIDATION_EXCEPTION,
                errors);
		return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = { ConstraintViolationException.class})
	protected ResponseEntity<MeetingPlannerException> handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {
		List<MeetingPlannerError> errors = new ArrayList<>();
		ex.getConstraintViolations().forEach(constraintViolation -> {
			errors.add(new MeetingPlannerError(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
		});
		MeetingPlannerException apiException =  new MeetingPlannerException(
				ex.getMessage(),
				HttpStatus.BAD_REQUEST,
				ErrorsKeysConstants.DATA_VALIDATION_EXCEPTION,
				errors);
		return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = { IllegalArgumentException.class})
	protected ResponseEntity<MeetingPlannerException> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
		MeetingPlannerException apiException =  new MeetingPlannerException(
				ex.getMessage(),
				HttpStatus.BAD_REQUEST,
				ex.getMessage(),
				null);
		return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
	}
}
