package com.meeting.planner.exceptions;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class MeetingPlannerError implements Serializable{

	private static final long serialVersionUID = 1L;
	private String i18nKey;
    private String value;
    private Map<String, Object> values = new HashMap<>();

    public MeetingPlannerError() {}

    public MeetingPlannerError(String i18nKey, String source) {
        this(i18nKey, source , new HashMap<>());
    }

    public MeetingPlannerError(String i18nKey, String value, Map<String, Object> values) {
        this.i18nKey = i18nKey;
        this.value = value;
        this.values = values;
    }
}
