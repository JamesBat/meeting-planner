package com.meeting.planner.exceptions;

public class ErrorsKeysConstants {

	public static final String USER_NOT_EXIST = "user not exist";
    public static final String ENTITY_NOT_FOUND = "Object introuvable";
    public static final String DATA_VALIDATION_EXCEPTION = "Échec de validation de données";

}
