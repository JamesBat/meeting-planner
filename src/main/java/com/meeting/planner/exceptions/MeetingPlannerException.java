package com.meeting.planner.exceptions;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MeetingPlannerException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private final LocalDateTime timestamp;
    private final HttpStatus httpStatus;
    private final String notification;
    private final String message;
    private final List<MeetingPlannerError> errors;

    public MeetingPlannerException(String message, HttpStatus status, String notification, List<MeetingPlannerError> errors) {

        this.timestamp = LocalDateTime.now();
        this.message = message;
        this.httpStatus = status;
        this.notification = notification;
        this.errors = errors;
    }

    public MeetingPlannerException(String message) {
        this(message, null, null, null);
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getNotification() {
        return notification;
    }

    public String getMessage() {
        return message;
    }

    public List<MeetingPlannerError> getErrors() {
        return errors;
    }

}
