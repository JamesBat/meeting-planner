package com.meeting.planner.services;

import java.util.List;

import com.meeting.planner.dtos.StageDto;

public interface StageService {
	List<StageDto> getAll();
}
