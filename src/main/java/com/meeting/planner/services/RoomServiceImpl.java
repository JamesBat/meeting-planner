package com.meeting.planner.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.meeting.planner.dtos.RoomDto;
import com.meeting.planner.entities.RoomEntity;
import com.meeting.planner.mappers.RoomMapper;
import com.meeting.planner.repositories.RoomRepository;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class RoomServiceImpl implements RoomService{

	private final RoomRepository roomRepository;
	private final RoomMapper roomMapper;

	@Override
	public RoomDto update(Integer id, RoomDto dto) {
		RoomEntity entity = roomMapper.updateRoom(dto, roomRepository.findById(id).get());
		return roomMapper.entityToDto(roomRepository.save(entity));
	}

	@Override
	public RoomDto getByName(String name) {
		return roomMapper.entityToDto(roomRepository.findByName(name));
	}

	@Override
	public List<RoomDto> getAll() {
		return roomRepository.findAll().stream().map(roomMapper::entityToDto).collect(Collectors.toList());
	}
}
