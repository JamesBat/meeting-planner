package com.meeting.planner.services;

import java.util.List;

import com.meeting.planner.dtos.ToolDto;

public interface ToolService {

	ToolDto add(ToolDto dto);
	ToolDto update(Integer id, ToolDto dto);
	List<ToolDto> getAll();
	ToolDto getByName(String name);
}
