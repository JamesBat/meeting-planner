package com.meeting.planner.services;

import java.util.List;

import com.meeting.planner.dtos.MeetingDto;

public interface MeetingService {

	MeetingDto add(MeetingDto dto);
	List<MeetingDto> getAll();
	MeetingDto getByName(String name);
}
