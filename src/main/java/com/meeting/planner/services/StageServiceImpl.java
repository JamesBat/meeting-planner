package com.meeting.planner.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.meeting.planner.dtos.StageDto;
import com.meeting.planner.mappers.StageMapper;
import com.meeting.planner.repositories.StageRepository;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class StageServiceImpl implements StageService{
	
	private final StageRepository stageRepository;
	private final StageMapper stageMapper;

	@Override
	public List<StageDto> getAll() {
		return stageRepository.findAll().stream().map(s -> stageMapper.entityToDto(s)).collect(Collectors.toList());
	}
}
