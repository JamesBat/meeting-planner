package com.meeting.planner.services;

import java.util.List;

import com.meeting.planner.dtos.RoomDto;

public interface RoomService {

	RoomDto update(Integer id, RoomDto dto);
	RoomDto getByName(String name);
	List<RoomDto> getAll();
}
