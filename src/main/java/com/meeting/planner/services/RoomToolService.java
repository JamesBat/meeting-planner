package com.meeting.planner.services;

import java.util.List;

import com.meeting.planner.dtos.RoomToolDto;

public interface RoomToolService {

	List<RoomToolDto> getAll();
	List<String> getToolByRoom(String roomName);
}
