package com.meeting.planner.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.meeting.planner.dtos.ToolDto;
import com.meeting.planner.entities.ToolEntity;
import com.meeting.planner.exceptions.MeetingPlannerException;
import com.meeting.planner.mappers.ToolMapper;
import com.meeting.planner.repositories.ToolRepository;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class ToolServiceImpl implements ToolService{
	
	private final ToolRepository toolRepository;
	private final ToolMapper toolMapper;

	@Override
	public ToolDto add(ToolDto dto) {
		if(toolRepository.findByName(dto.getName()) != null) throw new MeetingPlannerException("Cet outil esxiste deja");
		ToolEntity entity = toolMapper.dtoToEntity(dto);
		return toolMapper.entityToDto(toolRepository.save(entity));
	}

	@Override
	public ToolDto update(Integer id, ToolDto dto) {
		ToolEntity entity = toolMapper.updateTool(dto, toolRepository.findById(id).get());
		return toolMapper.entityToDto(toolRepository.save(entity));
	}

	@Override
	public List<ToolDto> getAll() {
		return toolRepository.findAll().stream().map(toolMapper::entityToDto).collect(Collectors.toList());
	}

	@Override
	public ToolDto getByName(String name) {
		return toolMapper.entityToDto(toolRepository.findByName(name));
	}
}
