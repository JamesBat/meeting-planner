package com.meeting.planner.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.meeting.planner.dtos.ReservationDto;
import com.meeting.planner.dtos.ReservationValueDto;
import com.meeting.planner.dtos.RoomDto;
import com.meeting.planner.entities.ReservationEntity;
import com.meeting.planner.enums.MeetingTypeEnum;
import com.meeting.planner.exceptions.MeetingPlannerException;
import com.meeting.planner.mappers.ReservationMapper;
import com.meeting.planner.mappers.RoomMapper;
import com.meeting.planner.repositories.ReservationRepository;
import com.meeting.planner.repositories.RoomToolRepository;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class ReservationServiceImpl implements ReservationService{
	
	private final ReservationRepository reservationRepository;
	private final ReservationMapper reservationMapper;
	private final RoomMapper roomMapper;
	private final RoomToolRepository rtRepository;
	
	private List<String> getToolByType(MeetingTypeEnum type){
		List<String> tools= new ArrayList<>();
		if(type.equals(MeetingTypeEnum.RC)) {
			tools.add("tableau");
			tools.add("ecran");
			tools.add("pieuvre");
		}else if(type.equals(MeetingTypeEnum.VC)) {
			tools.add("ecran");
			tools.add("pieuvre");
			tools.add("webcam");
		}else if(type.equals(MeetingTypeEnum.SPEC)) {
			tools.add("tableau");
		}
		return tools;
	}
	
	private List<RoomDto> getAvailableRoom(Integer capacity) {
		return rtRepository.getRoomByParam(capacity)
						   .stream()
						   .map(roomMapper::entityToDto)
						   .collect(Collectors.toList());
	}
	
	private Boolean checkIfAttemptTimeIsRespect(String roomName, LocalDateTime date) {
		List<ReservationDto> l = reservationRepository.getReservationByDate(roomName, date.withHour(8), date.withHour(20))
							 .stream()
							 .filter(r -> date.getHour() - r.getEndReservation().getHour() < 1)
							 .map(reservationMapper::entityToDto).collect(Collectors.toList());
		return (l.size() == 0);
	}
	
	private RoomDto getRoomDto(List<RoomDto> availableRooms, String type) {
		List<RoomDto> listValidByDefaultRessources = availableRooms.stream()
				   												   .filter(room -> rtRepository.getToolNameByRoomName(room.getName()).containsAll(getToolByType(MeetingTypeEnum.valueOf(type))))
				   												   .collect(Collectors.toList());
		if(listValidByDefaultRessources.size() > 0) return listValidByDefaultRessources.get(0);	
		return availableRooms.get(0);
	}
	
	@Override
	public ReservationDto add(ReservationDto dto) {
		if(!reservationRepository.getReservationsByMeeting(dto.getMeeting().getId()).isEmpty()) throw new MeetingPlannerException("Cette reunion à deja une reservation");
		List<RoomDto> availableRooms = getAvailableRoom(dto.getMeeting().getParticipant());
		if(availableRooms.isEmpty()) throw new MeetingPlannerException("Il n'y a pas de salle pouvant abriter ce nombre de participants");
		availableRooms = availableRooms.stream().filter(ar -> reservationRepository.getReservationByRoomParam(ar.getName(), dto.getMeeting().getStartMeeting(), dto.getMeeting().getEndMeeting()) == null).collect(Collectors.toList());
		if(availableRooms.isEmpty()) throw new MeetingPlannerException("Toutes les salles pouvant abriter ce nombre de participants sont deja reservées à ce créneau");
		availableRooms = availableRooms.stream().filter(ar -> checkIfAttemptTimeIsRespect(ar.getName(), dto.getMeeting().getStartMeeting())).collect(Collectors.toList());
		if(availableRooms.isEmpty())  throw new MeetingPlannerException("Cette reunion ne respecte pas le temps d'attente avec celle qui doit la précéder");
		
		RoomDto room = getRoomDto(availableRooms, dto.getMeeting().getType().name());
		
		ReservationDto reservationDto = new ReservationDto();
		reservationDto.setEnabled(true);
		reservationDto.setMeeting(dto.getMeeting());
		reservationDto.setRoom(room);
		reservationDto.setStartReservation(dto.getMeeting().getStartMeeting());
		reservationDto.setEndReservation(dto.getMeeting().getEndMeeting());
	
		return reservationMapper.entityToDto(reservationRepository.save(reservationMapper.dtoToEntity(reservationDto)));
	}
	
	@Override
	public void remove(Integer id) {
		ReservationEntity entity = reservationRepository.findById(id).get();
		entity.setEnabled(false);
		reservationRepository.save(entity);
	}

	@Override
	public List<ReservationValueDto> getAll() {
		return reservationRepository.getAll().stream().map(r -> {
			ReservationValueDto rv = new ReservationValueDto();
			rv.setMeetingName(r.getMeeting().getName());
			rv.setRoomName(r.getRoom().getName());
			rv.setParticipant(r.getMeeting().getParticipant());
			rv.setRoomCapacity(r.getRoom().getCapacity());
			rv.setStartTime(r.getStartReservation());
			rv.setEndTime(r.getEndReservation());
			return rv;
		}).collect(Collectors.toList());
	}

	@Override
	public ReservationValueDto getByMeeting(String meetingName) {
		ReservationEntity r = reservationRepository.getReservationByMeetinName(meetingName);
		ReservationValueDto rv = new ReservationValueDto();
		rv.setMeetingName(r.getMeeting().getName());
		rv.setRoomName(r.getRoom().getName());
		rv.setParticipant(r.getMeeting().getParticipant());
		rv.setRoomCapacity(r.getRoom().getCapacity());
		rv.setStartTime(r.getStartReservation());
		rv.setEndTime(r.getEndReservation());
		return rv;
	}
}
