package com.meeting.planner.services;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.meeting.planner.dtos.MeetingDto;
import com.meeting.planner.exceptions.MeetingPlannerException;
import com.meeting.planner.mappers.MeetingMapper;
import com.meeting.planner.repositories.MeetingRepository;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class MeetingServiceImpl implements MeetingService{
	
	private final MeetingRepository meetingRepository;
	private final MeetingMapper meetingMapper;
	
	private Boolean checkIfTimeSlotIsValid(MeetingDto dto) {
		List<String> validDays = Arrays.asList("MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY");
		List<String> days = Arrays.asList(dto.getStartMeeting().getDayOfWeek().name(), dto.getEndMeeting().getDayOfWeek().name());
		if(validDays.containsAll(days) && (dto.getStartMeeting().getHour() >= 8) && (dto.getEndMeeting().getHour() <=20) && (dto.getEndMeeting().getHour() - dto.getStartMeeting().getHour() == 1))
			return true;
		return false;
	}

	@Override
	public MeetingDto add(MeetingDto dto) {
		if(!checkIfTimeSlotIsValid(dto)) throw new MeetingPlannerException("Les informations de cette reunion ne sont pas valides", HttpStatus.NOT_ACCEPTABLE, null, null);
		return meetingMapper.entityToDto(meetingRepository.save(meetingMapper.dtoToEntity(dto)));
	}

	@Override
	public List<MeetingDto> getAll() {
		return meetingRepository.findAll().stream().map(meetingMapper::entityToDto).collect(Collectors.toList());
	}

	@Override
	public MeetingDto getByName(String name) {
		return meetingMapper.entityToDto(meetingRepository.findByName(name));
	}
}
