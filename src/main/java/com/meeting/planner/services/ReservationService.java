package com.meeting.planner.services;

import java.util.List;

import com.meeting.planner.dtos.ReservationDto;
import com.meeting.planner.dtos.ReservationValueDto;

public interface ReservationService {

	ReservationDto add(ReservationDto dto);
	void remove(Integer id);
	List<ReservationValueDto> getAll();
	ReservationValueDto getByMeeting(String meetingName);
}
