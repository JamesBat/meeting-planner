package com.meeting.planner.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.meeting.planner.dtos.RoomToolDto;
import com.meeting.planner.mappers.RoomToolMapper;
import com.meeting.planner.repositories.RoomToolRepository;

import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class RoomToolServiceImpl implements RoomToolService{

	private final RoomToolRepository roomToolRepository;
	private final RoomToolMapper roomToolMapper;
	
	@Override
	public List<RoomToolDto> getAll() {
		return roomToolRepository.findAll().stream().map(roomToolMapper::entityToDto).collect(Collectors.toList());
	}

	@Override
	public List<String> getToolByRoom(String roomName) {
		return roomToolRepository.getToolNameByRoomName(roomName);
	}

}
