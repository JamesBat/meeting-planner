package com.meeting.planner.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meeting.planner.dtos.ReservationDto;
import com.meeting.planner.services.ReservationService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("reservation")
@AllArgsConstructor
public class ReservationController {
	
	private final ReservationService reservationService;
	
	@PostMapping
	public ResponseEntity<ReservationDto> add(@RequestBody ReservationDto dto){
		return new ResponseEntity<>(reservationService.add(dto), HttpStatus.ACCEPTED);
	}
	
	@PutMapping("/remove/{id}")
	public ResponseEntity<Void> remove(@PathVariable("id") Integer id){
		reservationService.remove(id);
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("all")
	public ResponseEntity<?> getAll(){
		return new ResponseEntity<>(reservationService.getAll(), HttpStatus.OK);
	}
	
	@GetMapping("{meetingName}")
	public ResponseEntity<?> getByMeetingName(@PathVariable("meetingName") String meetingName){
		return new ResponseEntity<>(reservationService.getByMeeting(meetingName), HttpStatus.OK);
	}
}
