package com.meeting.planner.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meeting.planner.dtos.MeetingDto;
import com.meeting.planner.services.MeetingService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("meeting")
@AllArgsConstructor
public class MeetingController {

	private final MeetingService meetingService;
	
	@PostMapping
	public ResponseEntity<MeetingDto> add(@RequestParam MeetingDto dto){
		return new ResponseEntity<>(meetingService.add(dto), HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/all")
	public ResponseEntity<?> getAll(){
		return new ResponseEntity<>(meetingService.getAll(), HttpStatus.OK);
	}
	
	@GetMapping("{name}")
	public ResponseEntity<MeetingDto> getByName(@PathVariable("name") String name){
		return new ResponseEntity<>(meetingService.getByName(name), HttpStatus.OK);
	}
}
