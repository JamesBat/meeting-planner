package com.meeting.planner.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meeting.planner.services.StageService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("stage")
@AllArgsConstructor
public class StageController {

	private final StageService stageService;
	
	@GetMapping("/all")
	public ResponseEntity<?> getAll(){
		return new ResponseEntity<>(stageService.getAll(), HttpStatus.OK);
	}
}
