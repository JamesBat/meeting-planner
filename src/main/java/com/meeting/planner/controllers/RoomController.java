package com.meeting.planner.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meeting.planner.dtos.RoomDto;
import com.meeting.planner.services.RoomService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("room")
@AllArgsConstructor
public class RoomController {

	private final RoomService roomService;
	
	@PutMapping("{id}")
	public ResponseEntity<RoomDto> update(@PathVariable("id") Integer id, @RequestBody RoomDto dto){
		return new ResponseEntity<>(roomService.update(id, dto), HttpStatus.OK);
	}
	
	@GetMapping("/all")
	public ResponseEntity<?> getAll(){
		return new ResponseEntity<>(roomService.getAll(), HttpStatus.OK);
	}
	
	@GetMapping("{name}")
	public ResponseEntity<RoomDto> getByName(@PathVariable("name") String name){
		return new ResponseEntity<>(roomService.getByName(name), HttpStatus.OK);
	}
	
}
