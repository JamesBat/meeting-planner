package com.meeting.planner.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meeting.planner.services.RoomToolService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("roomTool")
@AllArgsConstructor
public class RoomToolController {

	private final RoomToolService roomToolService;
	
	@GetMapping("/tool/{roomName}")
	public ResponseEntity<?> getToolByRoom(@PathVariable("roomName") String roomName){
		return new ResponseEntity<>(roomToolService.getToolByRoom(roomName), HttpStatus.OK);
	}
}
