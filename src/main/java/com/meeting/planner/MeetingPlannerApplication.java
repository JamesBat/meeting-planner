package com.meeting.planner;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.meeting.planner.entities.MeetingEntity;
import com.meeting.planner.entities.RoomEntity;
import com.meeting.planner.entities.RoomToolEntity;
import com.meeting.planner.entities.StageEntity;
import com.meeting.planner.entities.ToolEntity;
import com.meeting.planner.enums.MeetingTypeEnum;
import com.meeting.planner.repositories.MeetingRepository;
import com.meeting.planner.repositories.ReservationRepository;
import com.meeting.planner.repositories.RoomRepository;
import com.meeting.planner.repositories.RoomToolRepository;
import com.meeting.planner.repositories.StageRepository;
import com.meeting.planner.repositories.ToolRepository;

@SpringBootApplication
public class MeetingPlannerApplication implements CommandLineRunner{

	@Autowired StageRepository stageRepository;
	@Autowired RoomRepository roomRepository;
	@Autowired ToolRepository toolRepository;
	@Autowired RoomToolRepository roomToolRepository;
	@Autowired MeetingRepository meetingRepository;
	@Autowired ReservationRepository reservationRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(MeetingPlannerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		StageEntity stage1 = new StageEntity(null,"E1",null);
		StageEntity stage2 = new StageEntity(null,"E2",null);
		StageEntity stage3 = new StageEntity(null,"E3",null);
		
		stageRepository.save(stage1);
		stageRepository.save(stage2);
		stageRepository.save(stage3);
		
		ToolEntity pieuvre = new ToolEntity(null, "pieuvre", 4, null);
		ToolEntity ecran = new ToolEntity(null, "ecran", 5, null);
		ToolEntity webcam = new ToolEntity(null, "webcam", 4, null);
		ToolEntity tableau = new ToolEntity(null, "tableau", 2, null);
		
		toolRepository.save(pieuvre);
		toolRepository.save(ecran);
		toolRepository.save(webcam);
		toolRepository.save(tableau);
		
		RoomEntity room1 = new RoomEntity(null, "E1001", 23, stage1, null, null);
		RoomEntity room2 = new RoomEntity(null, "E1002", 10, stage1, null, null);
		RoomEntity room3 = new RoomEntity(null, "E1003", 8, stage1, null, null);
		RoomEntity room4 = new RoomEntity(null, "E1004", 4, stage1, null, null);
		RoomEntity room5 = new RoomEntity(null, "E2001", 4, stage1, null, null);
		RoomEntity room6 = new RoomEntity(null, "E2002", 15, stage1, null, null);
		RoomEntity room7 = new RoomEntity(null, "E2003", 7, stage1, null, null);
		RoomEntity room8 = new RoomEntity(null, "E2004", 9, stage1, null, null);
		RoomEntity room9 = new RoomEntity(null, "E3001", 13, stage1, null, null);
		RoomEntity room10 = new RoomEntity(null, "E3002", 8, stage1, null, null);
		RoomEntity room11 = new RoomEntity(null, "E3003", 9, stage1, null, null);
		RoomEntity room12 = new RoomEntity(null, "E3004", 4, stage1, null, null);
		
		roomRepository.save(room1);
		roomRepository.save(room2);
		roomRepository.save(room3);
		roomRepository.save(room4);
		roomRepository.save(room5);
		roomRepository.save(room6);
		roomRepository.save(room7);
		roomRepository.save(room8);
		roomRepository.save(room9);
		roomRepository.save(room10);
		roomRepository.save(room11);
		roomRepository.save(room12);
		
		RoomToolEntity rt1 = new RoomToolEntity(null, room1, null, true);
		RoomToolEntity rt2 = new RoomToolEntity(null, room2, ecran, true);
		RoomToolEntity rt3 = new RoomToolEntity(null, room3, pieuvre, true);
		RoomToolEntity rt4 = new RoomToolEntity(null, room4, tableau, true);
		RoomToolEntity rt5 = new RoomToolEntity(null, room5, null, true);
		RoomToolEntity rt6 = new RoomToolEntity(null, room6, ecran, true);
		RoomToolEntity rt6_ = new RoomToolEntity(null, room6, webcam, true);
		RoomToolEntity rt7 = new RoomToolEntity(null, room7, null, true);
		RoomToolEntity rt8 = new RoomToolEntity(null, room8, tableau, true);
		RoomToolEntity rt9 = new RoomToolEntity(null, room9, ecran, true);
		RoomToolEntity rt9_ = new RoomToolEntity(null, room9, webcam, true);
		RoomToolEntity rt9__ = new RoomToolEntity(null, room9, pieuvre, true);
		RoomToolEntity rt10 = new RoomToolEntity(null, room10, null, true);
		RoomToolEntity rt11 = new RoomToolEntity(null, room11, ecran, true);
		RoomToolEntity rt11_ = new RoomToolEntity(null, room11, pieuvre, true);
		RoomToolEntity rt12 = new RoomToolEntity(null, room12, null, true);
		
		roomToolRepository.save(rt1);
		roomToolRepository.save(rt2);
		roomToolRepository.save(rt3);
		roomToolRepository.save(rt4);
		roomToolRepository.save(rt5);
		roomToolRepository.save(rt6);
		roomToolRepository.save(rt6_);
		roomToolRepository.save(rt7);
		roomToolRepository.save(rt8);
		roomToolRepository.save(rt9);
		roomToolRepository.save(rt9_);
		roomToolRepository.save(rt9__);
		roomToolRepository.save(rt10);
		roomToolRepository.save(rt11);
		roomToolRepository.save(rt11_);
		roomToolRepository.save(rt12);
		
		LocalDate date = LocalDate.now();
	    		
		MeetingEntity meeting1 = new MeetingEntity(null, "Reunion 1", date.atTime(9, 00), date.atTime(10, 00), 8, MeetingTypeEnum.VC, null);
		MeetingEntity meeting2 = new MeetingEntity(null, "Reunion 2", date.atTime(9, 00), date.atTime(10, 00), 6, MeetingTypeEnum.VC, null);
		MeetingEntity meeting3 = new MeetingEntity(null, "Reunion 3", date.atTime(11, 00), date.atTime(12, 00), 4, MeetingTypeEnum.RC, null);
		MeetingEntity meeting4 = new MeetingEntity(null, "Reunion 4", date.atTime(11, 00), date.atTime(12, 00), 2, MeetingTypeEnum.RS, null);
		MeetingEntity meeting5 = new MeetingEntity(null, "Reunion 5", date.atTime(11, 00), date.atTime(12, 00), 9, MeetingTypeEnum.SPEC, null);
		MeetingEntity meeting6 = new MeetingEntity(null, "Reunion 6", date.atTime(9, 00), date.atTime(10, 00), 7, MeetingTypeEnum.RC, null);
		MeetingEntity meeting7 = new MeetingEntity(null, "Reunion 7", date.atTime(8, 00), date.atTime(9, 00), 9, MeetingTypeEnum.VC, null);
		MeetingEntity meeting8 = new MeetingEntity(null, "Reunion 8", date.atTime(8, 00), date.atTime(9, 00), 10, MeetingTypeEnum.SPEC, null);
		MeetingEntity meeting9 = new MeetingEntity(null, "Reunion 9", date.atTime(9, 00), date.atTime(10, 00), 5, MeetingTypeEnum.SPEC, null);
		MeetingEntity meeting10 = new MeetingEntity(null, "Reunion 10", date.atTime(9, 00), date.atTime(10, 00), 4, MeetingTypeEnum.RS, null);
		MeetingEntity meeting11 = new MeetingEntity(null, "Reunion 11", date.atTime(9, 00), date.atTime(10, 00), 8, MeetingTypeEnum.RC, null);
		MeetingEntity meeting12 = new MeetingEntity(null, "Reunion 12", date.atTime(11, 00), date.atTime(12, 00), 12, MeetingTypeEnum.VC, null);
		MeetingEntity meeting13 = new MeetingEntity(null, "Reunion 13", date.atTime(11, 00), date.atTime(12, 00), 5, MeetingTypeEnum.SPEC, null);
		MeetingEntity meeting14 = new MeetingEntity(null, "Reunion 14", date.atTime(8, 00), date.atTime(9, 00), 3, MeetingTypeEnum.VC, null);
		MeetingEntity meeting15 = new MeetingEntity(null, "Reunion 15", date.atTime(8, 00), date.atTime(9, 00), 2, MeetingTypeEnum.SPEC, null);
		MeetingEntity meeting16 = new MeetingEntity(null, "Reunion 16", date.atTime(8, 00), date.atTime(9, 00), 12, MeetingTypeEnum.VC, null);
		MeetingEntity meeting17 = new MeetingEntity(null, "Reunion 17", date.atTime(10, 00), date.atTime(11, 00), 6, MeetingTypeEnum.VC, null);
		MeetingEntity meeting18 = new MeetingEntity(null, "Reunion 18", date.atTime(11, 00), date.atTime(12, 00), 2, MeetingTypeEnum.RS, null);
		MeetingEntity meeting19 = new MeetingEntity(null, "Reunion 19", date.atTime(9, 00), date.atTime(10, 00), 4, MeetingTypeEnum.RS, null);
		MeetingEntity meeting20 = new MeetingEntity(null, "Reunion 20", date.atTime(9, 00), date.atTime(10, 00), 7, MeetingTypeEnum.RC, null);
		
		meetingRepository.save(meeting1);
		meetingRepository.save(meeting2);
		meetingRepository.save(meeting3);
		meetingRepository.save(meeting4);
		meetingRepository.save(meeting5);
		meetingRepository.save(meeting6);
		meetingRepository.save(meeting7);
		meetingRepository.save(meeting8);
		meetingRepository.save(meeting9);
		meetingRepository.save(meeting10);
		meetingRepository.save(meeting11);
		meetingRepository.save(meeting12);
		meetingRepository.save(meeting13);
		meetingRepository.save(meeting14);
		meetingRepository.save(meeting15);
		meetingRepository.save(meeting16);
		meetingRepository.save(meeting17);
		meetingRepository.save(meeting18);
		meetingRepository.save(meeting19);
		meetingRepository.save(meeting20);
	}
}
