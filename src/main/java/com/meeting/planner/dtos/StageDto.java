package com.meeting.planner.dtos;

import java.io.Serializable;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StageDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	@NotNull(message = "Name is required")
	private String name;
}
