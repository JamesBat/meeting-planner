package com.meeting.planner.dtos;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.meeting.planner.enums.MeetingTypeEnum;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MeetingDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	@NotNull(message = "Name is required")
	private String name;
	
	@NotNull(message = "startMeeting is required")
	private LocalDateTime startMeeting;
	
	@NotNull(message = "endMeeting is required")
	private LocalDateTime endMeeting;
	
	@NotNull(message = "Number of participants is required")
	private Integer participant;
	
	@NotNull(message = "Meeting Type is required")
	private MeetingTypeEnum type;
}
