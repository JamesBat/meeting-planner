package com.meeting.planner.dtos;

import java.io.Serializable;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoomToolDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	@NotNull(message = "room is required")
	private RoomDto room;
	
	@NotNull(message = "room is required")
	private ToolDto tool;
	
	private Boolean enabled;
}
