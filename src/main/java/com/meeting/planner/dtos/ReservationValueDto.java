package com.meeting.planner.dtos;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReservationValueDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private String meetingName;
	private String roomName;
	private Integer participant;
	private Integer roomCapacity;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
}
