package com.meeting.planner.dtos;

import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReservationDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Boolean enabled;
	private LocalDateTime startReservation;
	private LocalDateTime endReservation;
	private RoomDto room;
	
	@NotNull(message = "meeting is required")
	private MeetingDto meeting;
}
