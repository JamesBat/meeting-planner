package com.meeting.planner.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RoomDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private List<RoomToolDto> roomTools = new ArrayList<>();
	
	@NotNull(message = "Name is required")
	private String name;
	
	@NotNull(message = "Capacity is required")
	private Integer  capacity;
	
	@NotNull(message = "Stage is required")
	private StageDto stage;
}
