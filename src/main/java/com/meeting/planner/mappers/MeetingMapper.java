package com.meeting.planner.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.meeting.planner.dtos.MeetingDto;
import com.meeting.planner.entities.MeetingEntity;

@Mapper(componentModel = "spring")
public interface MeetingMapper {

	MeetingDto entityToDto(MeetingEntity entity);
	
	@Mapping(target = "reservations", ignore = true)
	MeetingEntity dtoToEntity(MeetingDto dto);
	
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "reservations", ignore = true)
	MeetingEntity updateMeeting(MeetingDto dto,@MappingTarget MeetingEntity entity);
}
