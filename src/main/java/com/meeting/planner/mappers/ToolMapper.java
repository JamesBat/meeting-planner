package com.meeting.planner.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.meeting.planner.dtos.ToolDto;
import com.meeting.planner.entities.ToolEntity;

@Mapper(componentModel = "spring")
public interface ToolMapper {

	ToolDto entityToDto(ToolEntity entity);
	
	@Mapping(target = "roomTools", ignore = true)
	ToolEntity dtoToEntity(ToolDto dto);
	
	@Mapping(target = "roomTools", ignore = true)
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "name", ignore = true)
	ToolEntity updateTool(ToolDto dto,@MappingTarget ToolEntity entity);
}
