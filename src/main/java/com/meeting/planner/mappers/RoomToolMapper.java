package com.meeting.planner.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import com.meeting.planner.dtos.RoomToolDto;
import com.meeting.planner.entities.RoomToolEntity;

@Mapper(componentModel = "spring", uses = {RoomMapper.class, ToolMapper.class})
public interface RoomToolMapper {

	RoomToolDto entityToDto(RoomToolEntity entity);
	
	RoomToolEntity dtoToEntity(RoomToolDto dto);
	
	RoomToolEntity updateRoomTool(RoomToolDto dto,@MappingTarget RoomToolEntity entity);
}
