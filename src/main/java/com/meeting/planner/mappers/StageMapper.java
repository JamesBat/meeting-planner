package com.meeting.planner.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.meeting.planner.dtos.StageDto;
import com.meeting.planner.entities.StageEntity;

@Mapper(componentModel = "spring")
public interface StageMapper {

	StageDto entityToDto(StageEntity entity);
	
	@Mapping(target = "rooms", ignore = true)
	StageEntity dtoToEntity(StageDto dto);
	
	@Mapping(target = "rooms", ignore = true)
	@Mapping(target = "id", ignore = true)
	StageEntity updateStage(StageDto dto,@MappingTarget StageEntity entity);
}
