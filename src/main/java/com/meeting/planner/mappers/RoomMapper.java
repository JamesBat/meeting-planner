package com.meeting.planner.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.meeting.planner.dtos.RoomDto;
import com.meeting.planner.entities.RoomEntity;

@Mapper(componentModel = "spring", uses = {StageMapper.class})
public interface RoomMapper {

	@Mapping(target = "roomTools", ignore = true)
	RoomDto entityToDto(RoomEntity entity);
	
	@Mapping(target = "reservations", ignore = true)
	@Mapping(target = "roomTools", ignore = true)
	RoomEntity dtoToEntity(RoomDto dto);
	
	@Mapping(target = "reservations", ignore = true)
	@Mapping(target = "roomTools", ignore = true)
	RoomEntity updateRoom(RoomDto dto,@MappingTarget RoomEntity entity);
}
