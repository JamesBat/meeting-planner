package com.meeting.planner.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import com.meeting.planner.dtos.ReservationDto;
import com.meeting.planner.entities.ReservationEntity;

@Mapper(componentModel = "spring", uses = {RoomMapper.class, MeetingMapper.class})
public interface ReservationMapper {

	ReservationDto entityToDto(ReservationEntity entity);
	
	ReservationEntity dtoToEntity(ReservationDto dto);
	
	ReservationEntity updateReservation(ReservationDto dto,@MappingTarget ReservationEntity entity);
}
