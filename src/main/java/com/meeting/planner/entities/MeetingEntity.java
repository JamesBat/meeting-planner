package com.meeting.planner.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.meeting.planner.enums.MeetingTypeEnum;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "MEETING")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MeetingEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false, unique = true)
	private String name;
	
	@Column(nullable = false)
	private LocalDateTime startMeeting;
	
	@Column(nullable = false)
	private LocalDateTime endMeeting;
	
	@Column(nullable = false)
	private Integer participant;
	
	@Column(nullable = false)
	private MeetingTypeEnum type;

	@OneToMany(mappedBy = "meeting")
	private List<ReservationEntity> reservations = new ArrayList<>();
}
