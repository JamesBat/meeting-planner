package com.meeting.planner.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ROOMTOOL")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoomToolEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "room")
	private RoomEntity room;
	
	@ManyToOne
	@JoinColumn(name = "tool")
	private ToolEntity tool;
	
	@Column(nullable = false)
	private Boolean enabled;
}
