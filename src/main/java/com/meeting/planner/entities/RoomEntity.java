package com.meeting.planner.entities;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "ROOM")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RoomEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false, unique = true)
	private String name;
	
	@Column(nullable = false)
	private Integer  capacity;
	
	@ManyToOne
	@JoinColumn(name = "stage")
	private StageEntity stage;
	
	@OneToMany(mappedBy = "room")
	@Transient
	private List<ReservationEntity> reservations = new ArrayList<>();
	
	@OneToMany(mappedBy = "room")
	@Transient
	private List<RoomToolEntity> roomTools = new ArrayList<>();
}
