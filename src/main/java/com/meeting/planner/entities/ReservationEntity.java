package com.meeting.planner.entities;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "RESERVATION")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReservationEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private LocalDateTime startReservation;
	
	@Column(nullable = false)
	private LocalDateTime endReservation;
	
	@Column(nullable = false)
	private Boolean enabled;
	
	@ManyToOne
	@JoinColumn(name = "room")
	private RoomEntity room;

	@ManyToOne
	@JoinColumn(name = "meeting")
	private MeetingEntity meeting;
}
