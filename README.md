# Make an API to implement a meeting planner
The API has been implemented around the following technical stack:
- Java 17 as Language
- Spring 3.1.0 as Framework
- H2 as database
- Maven for dependency management
- Jpa & Hibernate for persistence and mapping of Database elements
- Junit 5 & Mockito for unit testing
- Mapstruct to map entities to Dto
- Swagger for documentation

## Code Availability
The source code is available on the Gitlab repository : [Meeting Planner](https://gitlab.com/JamesBat/meeting-planner ) 

## Branch Structure
The Repository includes 01 branch namely **main**

## Execution
The port on which the application is running is: 9001
The default identifiers of the BD H2 are
- url: jdbc:h2:mem:plannerdb
- login: sa
- password: password

When you launch the application the basic structure including the creation of floors, rooms, tools and scheduled meetings are saved.
You can find these elements on the file : `MeetingPlannerApplication.java` ( /meeting-planner/src/main/java/com/meeting/planner/MeetingPlannerApplication.java ) 

## Documentation
Documentation for webservices is available at : [documentation api](http://localhost:9001/swagger-ui.html)

#### Thank You